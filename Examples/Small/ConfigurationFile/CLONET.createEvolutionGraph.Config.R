## Path to CLONET
path_to_CLONET_functions <- ""

## Data cleaned for tumor evolution path construction
CleanData_RData <- "Examples/Small/Results/CleanDataForTEP.RData"

## PM clonality table file name
OutGraph_file <- "Examples/Small/Results/TumorEvolutionPath.graphml"

## List of genes to analyze: one column without header
#ListOfGenes_file <- "Examples/Small/BasicData/ListOfCandidateDriverGenesAnyCancer.IntoGen.csv"
ListOfGenes_file <- ""

## Genes annotation: tab separated file with information for each gene
genesInfo_file <- "Examples/Small/BasicData/genesInformations.hg19.csv"

## Define if early (clonalStatus) and late (subclonalStatus) aberrations
clonalStatus <- c("clonal","uncertain.clonal")
subclonalStatus <- c("subclonal","uncertain.subclonal")



## number of cores to use
ncores <- 2

