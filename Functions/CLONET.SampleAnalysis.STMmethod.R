source(paste0(path_to_CLONET_functions,"Functions/CLONET.STM.Functions.R"))

#####################################
##Analysis
outVal<-mclapply(samplesToAnalyse,
                 analyseSample.STMmethod,
                 samplesInfo = samplesInfo, #File with tumor/blood sample name
                 mutationList = mutationList, #Bed list of mutations to analyse (4 column for the name)
                 pileup_dir = pileup_dir, #Where PaPI pileup is located
                 analysis_dir = analysis_dir, #Where to save the analysis of the sample
                 minCoverage = minCoverage, #Min cov 
                 min_nsnps = min_nsnps, # min number of SNPs to call beta
                 PaPI_Suffix = PaPI_Suffix, #suffix of pileup files
                 n.digits=n.digits,#n significative digits
                 perSampleCores = perSampleCores,
                 mc.cores=NsamplesToProcessInParallel,
                 mc.preschedule=F)

#cat("!!HERE!!\n")
betaTable <- do.call(rbind,outVal)

